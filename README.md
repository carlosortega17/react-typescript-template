# React Typescript Template

### Install all dependencies

```bash
$ yarn
```

### Development template

```bash
$ yarn dev
```

### Check Lint

```bash
$ yarn lint
```

### Format code with Prettier

```bash
$ yarn format
```
