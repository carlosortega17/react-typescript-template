import React from 'react';

function App(): React.ReactElement {
  return (
    <div>
      <h1>Welcome</h1>
    </div>
  );
}

export default App;
